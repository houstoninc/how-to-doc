# How can you deploy a new instance of Eltel

## Create the new repository

- The very first step is to fork or clone the [eltel-resma](https://bitbucket.org/houstoninc/eltel-resma/src/master/) repository to start as a model
- Once is properly created, clone the newly created repository into your machine for private deployment and follow these steps below:
  - Create your `.env` file and read the _[Things to know know before](../Things-to-know-before/README.md)_ to learn about your variables
  - `$ npm run bootstrap` and follow instructions, note that if you don't create the `.env` file, with varibles included, prior running the script it will ask for the variables one by one, otherwise it will automatically run all the scripts contained in the `/npm-scripts/bootstrap.sh` file.
    - ### **Important note:** `if you are using Windows OS make sure you read the 'System Requirements' part on the` _[Things to know know before](../Things-to-know-before/README.md)_ `part of this documentation`
  - Now you can start developing with `$ npm run start`.
  - This will start a create-react-app **development server** in http://localhost:3000.
  - The said server is **local**, but it runs against your **private** development environment in _houston-aws-sandbox_.
  - And when you need to deploy a **new version** of your environment, use `$ npm run build-and-deploy` if you are a Mac IOS user. For Windows OS users make sure to run `npm run build-all-packages`, add `$ #!/usr/bin/env node` to your newly updated file `packages\cloud\infrastructure\build\index.js`, run `npx lerna run create-deployment-toolkit` and lastly `npm run deploy-all-packages`, your environment has been updated.

### Important note: `Keep in mind that your private environment will be created in the Houston-aws-sandbox`

![Eltel private stack](../Images/eltel_private_example.png)

## Once you are able to run your private environment.

- You can start developing the new instance of `Eltel`, make sure to read about the `Bitbucket Repository variables` and the variables you need to add in the `Deployments` section of the repository in the _[Things to know know before](../Things-to-know-before/README.md)_ part of this documentation.
- If new translations are needed you can create a `zip` file that will contain all the translation files that the app uses, this can be done by running `npm run export-translation-zip`. Note that if you are a Windows OS user you might have problems running this script, make sure you have `zip` package installed by doing `$ zip --version`, if your system does not find it install it with `$ sudo apt install zip`. Send that zip file to your contact at Eltel and ask them to add the respective language in all the files and send the updated zip file back to you.

- Make sure all the file are correct, place the `zip` file in the root of the project and add the updated files by running `npm run import-translation-zip`

  - Make sure all the respective files are updated for a succesful new language integration, such as
    - `packages/shared-between-front-ends/src/components/public/MeasurementAmountInput/measurementUnits.js`
    - `packages/shared-between-everything/src/workOrderTypes.js`
    - `packages/shared-between-everything/src/userRightValueObjects.js`
    - `packages/shared-between-front-ends/src/components/public/LanguageSelector/LanguageSelectorModel.js`
    - `packages/shared-between-front-ends/src/components/public/LanguageSelector/LanguageSelectorModel.test.js`
    - To make sure run `npm test`, study snapshot test fails, if any, they will provide more information.
  - If ou wish to set the new language as the default language in the `packages/shared-between-everything/src/defaultLanguage.js` file

- Other changes or updated might be needed but those must be agreed upon with the customer

## Deployment for staging and production

### In order to create the Staging and Production deployments we need to consider the following:

- Eltel is built & deployed using Bitbucket Pipeline
- Please read the _[Things to know know before](../Things-to-know-before/README.md)_ file to learn everything about the _`Repository`_ and _`Deployments`_ variables
- **Run** the pipeline **manually** to get it running the first time. Later new commits will trigger this automatically.
- Always work locally with your own branch, is always best practice to **pull-requests**. Else `$ git push` command will only run pre-checks but won't allow you to push your code directly to the master branch
- These deployments run in three different AWS zones: `us-east-1`, `eu-central-1` and `eu-west-1` and the following resources are created in the AWS console: `AWS Lambda`, `CloudFront::Distributions`, `CloudFormation::Stacks`, `DynamoDB`, `AWS Certificate Manager` to manually create a new certificate for the new instance, `S3::Buckets` and `Route53::HostedZones`

  ![Eltel pipeline](../Images/eltel_pipeline.png)
  Eltel build contains Staging and Production

  ![Repository variables](../Images/eltel_variables.png)
  Configure repository variables. See Eltel Finland or Sweden for Example.

  ![Build variables](../Images/eltel_build_variables.png)
  Configure more variables under Deployment, pay attention to APPLICATION_NAME. See Eltel Finland or Sweden for Example.

### Accessing staging and production

- Staging and Production are built under Eltel AWS account to zones `us-east-1` and `eu-west-1`
- Staging is named eltel-[country]-master-resma
- Production is named eltel-[country]-production-resma

  ![Stacks](../Images/eltel_account_stacks.png)
  Stacks

  ![Cloud front](../Images/eltel_account_cloudfront.png)
  Cloudfront

  ![Route53](../Images/houston_account_route53.png)
  Domain for Staging is added automatically to Houston sandbox Route53. Domain for production should be
  configured by Eltel.

### Azure AD Authentication

For Microsoft Azure AD authentication to work, allowed domains must be added to Eltel Azure AD by Eltel IT.
Otherwise Microsoft Azure AD will give an error before the front page is even loaded.
It is recommended to add those domain names by the `Record name` found at **_AWS::Route53::HostedZones_** of the **houston-aws-sandbox** or by the `Alternate domain names` found at **_AWS::CloudFront::Distributions_** of the **Eltel-aws console**, the reason is that if a distribution is deleted and recreated the Domain name will change but the the Record Name and the Alternate domain name will always be the same.

### First user to have permission to edit other users

User permissions are maintained by the application in DynamoDB tables eltel-[country]-master-resma-users
for staging and eltel-[country]-production-resma-users for production. There is a need to give the first
"admin" user permissions manually to add/edit other users

- Go to DynamoDB `eu-west-1` of Eltel AWS
- Go to Tables
- Look for eltel-finland-production-resma-users
- Click on view items button
- Look for user that has all the rights, example: niko.lindholm@eltelnetworks.com
- Copy this row to eltel-[country]-master-resma-users and eltel-[country]-production-resma-users with the email address that you prefer to give permissions
- Now you can see "Users" in application menu and start adding/editing users using the application. There is no more need to manually edit DynamoDB tables

### Go to _Users_ in the menu

![ResmaMenu](../Images/ResmaMenu.png)

### Here you can create a new user and by checking the boxes you can add or remove user's rights, as long as the user has 'Mantain Users' rights

![UsersPage](../Images/UsersPage.png)
