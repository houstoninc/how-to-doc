# Get started, get ready.

## This repository will guide you through pretty much everything you need to know before either launching your private environment or deploying a new production environment of Eltel.

### I recommend you go through this documentation in the following order:

1. [Thing to know before](./Things-to-know-before/README.md),
1. [Get-Started](./Get-Started/README.md), and last but not least
1. [How to deploy a new instance](./How-to-deploy-a-new-instance/README.md)


> if you have any questions, suggestion or would like to report typos, do not hesitate to get in contact with the

![Igniter](./Images/igniter.jpeg)

># _`Igniter Team`_