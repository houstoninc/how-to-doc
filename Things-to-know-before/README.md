# Some of these are just recommendations and some are requirements

## System requirements

- Mac IOS is recommended to work on this project, but if you have a Windows keep in mind you will need to set some things up.
- If you use Windows you will need to install [Ubuntu on WSL](https://ubuntu.com/wsl) and then [Node.js](https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl), if this environment is not setup the scripts in the package.json files won't be properly read, will cause fails, and the shell files won't properly read the scripts inside of them.
- Keep in mind that you will need to add a script such as `$ #!/usr/bin/env node` in the first line of a certain file so that this said file won't be read as an script. Not adding this would cause a fail during the bootstraping process.

## Environment variables

### For our private developemt environment the `.env` file needs to include:

- AWS_DEFAULT_REGION=eu-central-1
- AWS_ACCESS_KEY_ID={your_access_key_id} **From your houston-aws-sandbox account**
- AWS_SECRET_ACCESS_KEY={your_secret_access_key} **From your houston-aws-sandbox account**
- OAUTH_CLIENT_SECRET={your_oauth_client_secret} **Ask igniter team or refer to the images for more detail if it is for an Eltel project**
- ACCEPTANCE_TESTING_SECRET=hard-coded-backdoor-secret

### Bitbucket repository variables for staging and production

- INLINE_RUNTIME_CHUNK = false
- VISMA_API_KEY = **Uknown at the moment**
- ACCEPTANCE_TESTING_SECRET = 13208h9sdfbkjsaddkjdwASd
- ELTEL_DATA_VAULT_API_KEY --> **`from eltel development team but ask igniter team to provide it for you`**
- ELTEL_DATA_VAULT_URL --> **`from eltel development team but ask igniter team to provide it for you`**
- OAUTH_CLIENT_SECRET --> **`from eltel development team but ask igniter team to provide it for you`**. Needed for Microsoft user authentication system integration.
- AWS_DEFAULT_REGION = eu-west-1
- AWS_SECRET_ACCESS_KEY & AWS_ACCESS_KEY_ID:

  > this variables are used for staging and production deployment and fetched from the Eltel AWS [Console](https://console.aws.amazon.com/iamv2/home?#/users) located in the Security Credentials page. If you are creating a instance please refer to this [how to create a new instance](../How-to-deploy-a-new-instance\README.md) documentation

### **IMPORTANT NOTE:** `beware that also Auth0 Authentication system is integrated into the app but it should not be use unless is just for bare testing deployment. Ask igniter team if you wish to learn more.`

### Bitbucket Deployment Variables

- For Test, Staging and Deployment `ELTEL_DATA_VAULT_API_KEY` and `ELTEL_DATA_VAULT_URL` variables are needed for the Eltel data vault integration and they ARE NOT the same values as they do in the Bitbucket environment variables. Also, **notice** that in **`Deployments`** the variables values are different for _`Test/Staging`_ and for _`Production`_, ask **Igniter Team** for more details if needed.
- For Production there is a variable called `APPLICATION_NAME` that has a value eltel-production or eltel-sweden-production, depending on the repository. In case we are creating a new instance refere to [how to create a new instance](../How-to-deploy-a-new-instance\README.md) documentation

# In the following screenshots you can see exactly how the variables are presented on Azure AD application from Eltel

### Client ID `ClientID`

![Client ID](../Images/ClientID.png)

### Client Secret `OAUTH_CLIENT_SECRET`

![Client Secret](../Images/ClientSecret.png)

### Endpoints `Need to be added to the resma/system-configuration.json file`

![End Points](../Images/Endpoints.png)

### URL's added to their app `This image might need to be updated if a new endpoints are added or edited`

![URLs Added](../Images/URLsAdded.png)

## Setting for VS code.

> Recommended setting for vs code can be found in the [setting.json](../Things-to-know-before/vscode-settings.json) file

## Data Vault integration

Eltel Data Vault is a simple REST interface for work orders. 

Things to know about the Data Vault integration:
- Each country has its own `ELTEL_DATA_VAULT_URL` & `ELTEL_DATA_VAULT_API_KEY`
- There are two separate endpoints. One for Testing/Staging, and one for Production. Eltel will provide these for you.
- Lambda `eltel-{country}-{environment}-resma-lambdas-command-processor` copies work orders from Data Vault to DynamoDB table `eltel-{country}-{environment}-resma-work-orders`
- Mapping of data is performed in `foreignToDomesticWorkOrderMapper.js` and may vary between countries. First manually query data from Data Vault and then adjust mappings
- Data Vault copy is done hourly, and status can be seen from CloudWatch log `eltel-{country}-{environment}-production-resma-lambdas-command-processor`
- It is a good idea to configure Data Vault URL, KEY and mapping before the deployment because of the copy. In case you deploy the application with some other country's details or incorrect mappings Lambda will copy incorrect data to DynamoDB and you have to clean it up later
- Data Vault integration is used for other requests as well, that are not documented here

> curl --location --request GET 'https://eai.eltelnetworks.com:9995/api/Resma/EltelDataVault/select' --header 'x-apikey: {ELTEL_DATA_VAULT_API_KEY}'

Example curl for Eltel Finland Data Vault

![Vault example](../Images/eltel_finland_vault.png)

Example response from Eltel Finland Data Vault.

![Work order copy status](../Images/eltel_work_order_copy.png)

Example status report of work orders copy from Eltel Finland. `Foreign work orders` are resolved from Data Vault, `domestic work orders` are resolved from DynamoDB

### See imported work orders in Resma application

- Open DynamoDB table `eltel-{country}-{environment}-resma-work-orders` in AWS console
- Pick any `department` ID
- Go to Eltel Resma application UI
- Create team or edit team to have same department id you picked from DynamoDB
- Then you can see work orders imported from Data Vault in Eltel Resma application UI
- Department IDs in this example are not matching, you have to live with that

![Example_departments](../Images/eltel_department.png)

Work orders in DynamoDB, see department ID

![Department ID in Eltel UI](../Images/eltel_department_ui.png)

Department ID in Eltel UI, ensure that one of them matches with DynamoDB department IDs. In case not, create new Team or edit existing team.

![Work orders in UI](../Images/eltel_work_orders_ui.png)

Now you can see work orders imported from Data Vault in the UI

## Commiting you code

- In order to successfully commit your code make sure you are working on `your own branch` and `not the master branch`, else you will be able to run pre-checks but won't be able to push your changes
- If the pre-push checks fail make sure you code style is checked by running `$ npm run fix-code-style` and try again,
- If the pre-push checks fail again, run `$ npm test` and make sure local unit test pass with no issues, including sanptshots,
- You should now be able to push your code but...
- If the pre-push checks still fails... `Houston We Have A Problem`.
