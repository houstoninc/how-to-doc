# Houston Igniter

![Igniter](../Images/igniter.jpeg)

## What is it?

- It is an **opinionated full-stack project bootstrap**.
- It favors **JavaScript** for front-end (ie. a website) and back-end (ie. some API's).
- It **automates** a lot of stuff to get **big things** done by **small teams**.
- It has following **features**:
  - CI/CD - a lot of stuff happens automatically: tests, builds, deployments, and etc.
  - Test automation - unit-, integration- and end-to-end -testing.
  - Authentication - you need to login eg. to see restricted features.
  - Front-end - using create-react-app.
  - UI-components with a flavor of design system.
  - Developer dashboard - showing the status of end-to-end test reports.
  - Back-end - using AWS Lambda.
  - Database - currently using AWS DynamoDB.
  - Error monitoring - using AWS SNS.
  - Code style consistency - using prettier and enforced linting.
  - A reference application - to be used as a starting point.
- It has a lot of stuff, however you can **opt-out**.

## What is it not?

- A framework.
- Cloud provider agnostic.
- Something that does everything out of the box.

## What can I do with it?

Well, we suppose you could, like, use it to:

- Get a running start with a **serious project**.
  > After first day of development, we showed customer something meaningful to try out. Agile! - The Igniter team.
- Get a running start with a **not so serious project**.
- **Study** stuff like TDD, React or a specific architecture.
- Build a **sales demo or POC**.
- **Steal** the good parts from it.
- **Help** making it better. Give some love for the team e.g. [JanneS](https://www.flowdock.com/app/private/201896) and [MikkoA](https://www.flowdock.com/app/private/245945).

## Where can I see the reference application?

- [Here](http://production-happy-farm-houston-igniter.igniter.houston.io).

## Where can I see the reference developer dashboard?

- [Here](http://developer-dashboard-houston-igniter.igniter.houston.io/).

## How do I try it out?

- Read the _[Things to know know before](../Things-to-know-before/README.md)_ to learn about your `.env` variables
- `$ npm run bootstrap` and follow instructions, note that if you don't create the `.env` file, with varibles included, prior running the script it will ask for the variables one by one, otherwise it will automatically run all the scripts contained in the `/npm-scripts/bootstrap.sh` file.
  - This will most importantly create a **private** environment for you in Houston-aws-sandbox.
- Now you can start developing with `$ npm run start`.
  - This will start a create-react-app **development server** in http://localhost:3000.
  - The said server is **local**, but it runs against your **private** development environment in AWS.
- And when you need to deploy a **new version** of your environment, use `$ npm run build-and-deploy`.

### **IMPORTANT NOTE:** `if you are using Windows OS make sure you read the` _`'System Requirements'`_ `part on the` _[Things to know know before](../Things-to-know-before/README.md)_ `section of this documentation.`

## How do I learn to get stuff done with it?

- Study the **unit tests**, as those work also as documentation that is up to date by contract:
  - The name of the test shows the **impact in the real world**.
  - The code of the test shows a **code example to inflict the said impact**.
- Study the **NPM-scripts**. A lot of automation for rudimentary tasks are here.
- For real, ask from the team e.g. `Paco` _(paco.zavala@houston-inc.com)_ or `Tommi` _(tommi.tarkki@houston-inc.com)_. Asking is professional.

## How do I start a full project with it?

### TL;DR:

- Fork the [Igniter](https://bitbucket.org/houstoninc/houston-igniter).
- Enable build pipelines and set some environment variables.
- Trigger the pipeline for the first time.
- Done.

### Non-TL;DR:

- **Fork** the [Igniter](https://bitbucket.org/houstoninc/houston-igniter). You will need to specify **your-project-owner**.
- **Enable** BitBucket **pipelines** to get the CI/CD running.
  - If you can't find how, the URL is something like this: https://bitbucket.org/**your-project-owner**/**your-fork**/admin/addon/admin/pipelines/settings.
- Set **environment variables** in BitBucket to permit access for doing deployment:
  - Create **AWS IAM user** with policy "**AdministratorAccess**" (less liberal policy is omitted) in [here](https://console.aws.amazon.com/iam/home?#/users).
  - Make note of the **Access Key** and **Secret Access Key**, as these are needed soon.
  - Go to **pipeline variables** or https://bitbucket.org/**your-project-owner**/**your-fork**/admin/addon/admin/pipelines/repository-variables.
  - Read the _[Things to know know before](../Things-to-know-before/README.md)_ to learn about `Bitbucket Repository` variables
- **Run** the pipeline **manually** to get it running the first time. Later new commits will trigger this automatically.
  - If you can't find how, the URL is something like this: https://bitbucket.org/**your-project-owner**/**your-fork**/branches/.
- **Wait** for the pipeline to finish. It is doing stuff like:
  - Verification of code style.
  - Verification of unit tests.
  - Verification of test coverage.
  - Building of front-end.
  - Building of back-end.
  - Deployment of front-end (ie. create-react-app hosted in AWS S3).
  - Deployment of back-end (ie. AWS Lambdas behind AWS API Gateway).
  - Deployment of databases (ie. AWS DynamoDB).
- **Ask** the Igniter team to add your application to allowed origins in Auth0.
- **Witness** the application working by opening http://**your-name**-**your-system-name**-**your-project-package-name**.igniter.houston.io.
- Done.

## What does it mean to create a new branch with it?

- Once a project is started as stated above, all **branches** will be deployed as **independent environments**.
- Eg. creating and pushing a branch called "**your-branch**" will result in working application in http://**your-branch-name**-**your-system-name**-**your-project-package-name**.igniter.houston.io
  - Notice how, in the URL, branch "**master**" from first deployment was replaced with "**your-branch**".
- The new environment deploys **independent instances** of front-end, back-end, database and etc.
- The new branches can be used for stuff like:
  - Feature-branches.
  - Hot-fix-branches.
  - Production-candidate/staging branches.
  - The actual production branch.
  - Etc.

## What are the technologies behind it?

- Create React App - for UI.
- MobX - for state management.
- SASS Modules - for styles.
- Jest + Enzyme - for unit testing.
- Lodash/fp - for not having to code everything from ground up.
- Cypress - for e2e-testing.
- Cucumber - for BDD/ATDD specifications.
- BitBucket Pipelines - for continuous integration and deployment.
- AWS: S3, API Gateway, Lambda, DynamoDB, CloudWatch, CloudFormation, SNS, CDK (Cloud Development Kit).
- OAuth & Auth0 - for authorization and authentication.
- Husky - to catch broken builds before made public.
- Lerna - for management of mono-repo.

## What are the buzz-words behind it?

- TDD
- BDD
- ATDD
- Snapshot testing
- [Clean code](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
- Agile
- Continuous experimentation
- Pragmatism
- DevOps
- GitOps
- Automate everything
- Monitoring
- Mono-repo
- Functional programming
- Serverless
- Sustainable velocity
- Minimalism
- Extreme programming
- Pair programming
- Refactoring
- KISS
- Craftsmanship
- [Uncle Bob](https://i.imgur.com/ZM4bo8s.png)

## How do I create an account that can be used to login with it?

- You can create an account with your email address or log in by using Google account .
